<a class="imagegradient" data-mode="grid" href="/imagegradients/01_17_IMG_6262WEB.jpg">01_17_IMG_6262WEB.jpg</a>
<script src="/lib/imagegradient.js"></script>
<script>
(function ($) { $(function () {
$("a.imagegradient").text("").each(function () {
    var href = $(this).attr("href");
    window.imagegradient.call(this, {
        src: href,
        gradient_src: href.replace(/\.jpg$/, ".gradient.png")
    });
})
}); })(jQuery);
</script>
Image gradients are a fundamental transformation used in image processing, search indexing, and computer vision. Like a weather map showing the direction and strength of the wind, an image gradient depicts the strength and direction of changes in intensity over the surface of the image.
<!--more-->
Through a fluke in my interpretation / understanding of the technique the gradients shown here on of images of objects from the Guttormsgaard archive, follow not the direction of the change but rather its perpendicular. As a result, the arrows chase around the edges of objects rather than piercing straight into them. Indeed, the image gradients shares a close affiliation between with the operation of edge-detection (where the movements are further segmented into discrete segments).

<a class="imagegradient" data-mode="grid" href="/imagegradients/01_25_IMG_6270WEB.jpg">01_25_IMG_6270WEB.jpg</a>
<a class="imagegradient" href="/imagegradients/01_40_IMG_6294WEB.jpg">01_40_IMG_6294WEB.jpg</a>
<a class="imagegradient" href="/imagegradients/12_IMG_2406WEB.jpg">12_IMG_2406WEB.jpg</a>
<a class="imagegradient" href="/imagegradients/12c_IMG_5525WEB.jpg">12c_IMG_5525WEB.jpg</a>
<a class="imagegradient" href="/imagegradients/13a_MG_8326WEB.jpg">13a_MG_8326WEB.jpg</a>
<a class="imagegradient" href="/imagegradients/15_IMG_2418WEB.jpg">15_IMG_2418WEB.jpg</a>
<a class="imagegradient" href="/imagegradients/18_IMG_2430WEB.jpg">18_IMG_2430WEB.jpg</a>
<a class="imagegradient" href="/imagegradients/19a_IMG_0424Web.jpg">19a_IMG_0424Web.jpg</a>

Following the description of Image derivatives in the book <a href="http://programmingcomputervision.com/">Programming Computer Vision with Python</a> (p. 18), the following python code uses gaussian filters to approximate the derivative (rate of change) in first the x and the the y directions, and then uses a square root function to find the magnitude of change, and an arc tangent function to find the direction (or its perpendicular) at each point. The results are used to construct a second "gradient image" where the pixels color (hue in degrees on the HSL color wheel) represent the direction, and the value (or lightness) the magnitude of change.

On this page, clicking on the image switches between 3 views: (1) drawing the gradient at the mouse location, (2) drawing a grid of equally spaced gradients, and (3) showing the underlying (and pre-calated) "hsl" gradient image, which is the source of data used to perform the first two views.

