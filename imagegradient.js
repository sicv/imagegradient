(function () {
    function $make (selector, parent, opts) {
        // Calls parent.querySelector(selector)
        // Creates new element and appends to parent if missing
        // requires:
        //   selector must include a tagname and optionally be qualified by a classname
        //
        // This function is designed to be used in documents that may be saved as a static document
        // thus allowing DOM elements to be "reclaimed" by javascript rather than re-creating them
        //
        var m = selector.match(/^(.+?)(?:\.(.+))?$/),
            tagname = m[1],
            classname = m[2];

        opts = (opts === undefined) ? {} : opts;
        // If parent is not already a DOM node, resolve as selector in document context
        if (!parent.nodeName) { parent = document.querySelector(parent); }
        var ret = parent.querySelector(selector);
        if (ret === null) {
            // CREATE ELEMENT, append to parent
            ret = document.createElement(tagname);
            parent.appendChild(ret);
            if (classname) {
                ret.setAttribute("class", classname);    
            }
        }
        if (opts.html) { ret.innerHTML = opts.html };
        // if (opts.text) { ret.textContent = opts.text };
        if (opts.style) {
            for (var s in opts.style) {
                ret.style[s] = opts.style[s];
            }
        }
        if (opts.attr) {
            for (var a in opts.attr) {
                ret.setAttribute(a, opts.attr[a]);
            }
        }
        return ret;
    }

  function imagegradient () {
    var elt = this,
      source_a = this.querySelector("a.source"),
      source_href = source_a.getAttribute("href"),
      gradient_a = this.querySelector("a.gradient"),
      gradient_href = gradient_a.getAttribute("href"),
      MODE_LIVE = 0,
      MODE_GRID = 1,
      MODE_IMAGE = 2,
      img, gimg, canvas, ctx, image_data,
      magScale = 100.0,
      grid_size = 10.0,
      width, height, ready = false,
      display_mode = MODE_LIVE;

    elt.style.position = "relative";
    elt.style.display = "inline-block";

    source_a.style.display = "none";
    gradient_a.style.display = "none";

    img = $make("img.source", elt, {'attr': {
        src: source_href
    }});
    img.addEventListener("load", init_with_size, false);

    function init_with_size () {
      // console.log("loaded");
      width = img.width;
      height = img.height;

      // console.log("load", w, h);
      canvas = $make("canvas.imagegradient_draw", elt, {
        style: {
            position: "absolute",
            left: "0px",
            top: "0px",
        },
        attr: {
            width: width,
            height: height
        }
      })
      canvas.addEventListener("mousemove", function (e) {
          if (!ready || display_mode!==MODE_LIVE) return;
          var cp = canvas.getBoundingClientRect(),
            mp = {
              x: Math.floor(e.clientX - cp.left),
              y: Math.floor(e.clientY - cp.top)
            };

          ctx.clearRect(0, 0, width, height);
          draw_gradient(mp.x, mp.y);
        }, false);

      canvas.addEventListener("mouseleave", function (e) {
          if (display_mode!==MODE_LIVE) return;
          ctx.clearRect(0, 0, width, height);
        }, false);

      canvas.addEventListener("click", function (e) {
          e.preventDefault();
          display_mode += 1;
          if (display_mode > MODE_IMAGE) display_mode = 0;
          set_mode(display_mode);
        }, false);

      function set_mode (m) {
        display_mode = m;
        if (display_mode == MODE_GRID) {
          draw_grid();
        } else if (display_mode == MODE_IMAGE) {
          draw_image();
        } else {
          ctx.clearRect(0, 0, width, height);
        }
      }

      ctx = canvas.getContext("2d");
      ctx.lineWidth = 1;
      ctx.strokeStyle = "rgba(255,0,255,1)";

      gimg = document.createElement("img");
      elt.appendChild(gimg);

      gimg.className = "imagegradient_gradient";
      gimg.style.display = "none";
      gimg.setAttribute("src", gradient_href);
      gimg.style.position = "absolute";
      gimg.style.left = "0px";
      gimg.style.top = "0px";
      gimg.style.display = "none";
      gimg.addEventListener("load", function () {
          ctx.drawImage(gimg, 0, 0);
          image_data = ctx.getImageData(0, 0, width, height);
          ctx.clearRect(0, 0, width, height);
          ready = true;
          // set initial mode
          var mode = elt.getAttribute("data-mode");
          if (mode === "grid") {
            set_mode(MODE_GRID);
          } else if (mode === "image") {
            set_mode(MODE_IMAGE);
          }
        }, false);

    }

    function draw_grid () {
      ctx.clearRect(0, 0, width, height);
      for (var y=0; y<height; y+= grid_size) {
          for (var x=0; x<width; x+= grid_size) {
              draw_gradient(x, y); } }
    }

    function draw_image () {
      ctx.clearRect(0, 0, width, height);
      ctx.drawImage(gimg, 0, 0);
    }

    // from d3/src/color/rgb.js
    function rgb_hsl(r, g, b) {
      var min = Math.min(r /= 255, g /= 255, b /= 255),
          max = Math.max(r, g, b),
          d = max - min,
          h,
          s,
          l = (max + min) / 2;
      if (d) {
        s = l < .5 ? d / (max + min) : d / (2 - max - min);
        if (r == max) h = (g - b) / d + (g < b ? 6 : 0);
        else if (g == max) h = (b - r) / d + 2;
        else h = (r - g) / d + 4;
        h *= 60;
      } else {
        h = NaN;
        s = l > 0 && l < 1 ? 0 : h;
      }
      return {h:h, s:s, l:l};
    }

    function draw_gradient(x, y) {
        var d = image_data.data,
          id_offset = ((y*image_data.width)+x)*4,
          hsl = rgb_hsl(d[id_offset+0], d[id_offset+1], d[id_offset+2]),
          hue = isNaN(hsl.h) ? 0 : hsl.h,
          level = isNaN(hsl.l) ? 0 : hsl.l,
          g_angle = (hue/360.0) * Math.PI * 2.0 - (Math.PI/2),
          g_mag = level * magScale,
          dy = -Math.cos(g_angle) * g_mag,
          dx = Math.sin(g_angle) * g_mag;
        ctx.beginPath();
        canvas_arrow(ctx, x, y, x+dx, y+dy);
        ctx.closePath();
        ctx.stroke();
    }
  }

  //http://stackoverflow.com/questions/808826/draw-arrow-on-canvas-tag
  function canvas_arrow(context, fromx, fromy, tox, toy){
    var headlen = 5;   // length of head in pixels
    var angle = Math.atan2(toy-fromy,tox-fromx);
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
  }

  window.imagegradient = imagegradient; 

  /* Activate elements of class imagegradient */

  var items = document.querySelectorAll(".imagegradient");
  for (var i=0, len=items.length; i<len; i++) {
    imagegradient.call(items[i]);
  }

})();