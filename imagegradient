#!/usr/bin/python

from PIL import Image
from numpy import *
from scipy.ndimage import filters
import sys, json, os, math
import argparse


def hsl_to_rgb (h, s, l):
    # fix ranges for hsl values
    if h == None:
        h = 0
    if h < 0 or h > 360:
        h = h % 360
    if s < 0:
        s = 0
    s = max(0.0, min(1.0, s))
    l = max(0.0, min(1.0, l))

    # From FvD 13.37, CSS Color Module Level 3
    if l <= .5:
        m2 = l * (1 + s)
    else:
        m2 = l + s - l * s
    m1 = 2 * l - m2

    def v(h):
        if (h > 360):
            h -= 360
        elif (h < 0):
            h += 360
        if (h < 60):
            return m1 + (m2 - m1) * h / 60
        if (h < 180):
            return m2
        if (h < 240):
            return m1 + (m2 - m1) * (240 - h) / 60
        return m1

    def vv(h):
        return round(v(h) * 255)

    return (int(vv(h + 120)), int(vv(h)), int(vv(h - 120)))


parser = argparse.ArgumentParser(description='Calculate an image gradient.')
parser.add_argument('input', help='an image path as input')
parser.add_argument('--format', default="hslimage", help='save output format, default hslimage (direction is hue, magnitude is lightness). Other options: magimage (magnitude image), json.')
parser.add_argument('--output', help='save to output path')
args = parser.parse_args()

# Based on Programming Computer Vision, chapter 1, Image Derivatives

p = args.input
path, base = os.path.split(p)
base, ext = os.path.splitext(base)
out = args.output or os.path.join(path, base + ".gradient.png")
im = array(Image.open(p).convert('L'))

# Sobel derivative filters
# imx = zeros(im.shape)
# filters.sobel(im,1,imx)
# imy = zeros(im.shape)
# filters.sobel(im,0,imy)
# magnitude = sqrt(imx**2+imy**2)

sigma = 5 # standard deviation
imx = zeros(im.shape)
filters.gaussian_filter(im, (sigma,sigma), (0,1), imx)
imy = zeros(im.shape)
filters.gaussian_filter(im, (sigma,sigma), (1,0), imy)
magnitude = sqrt(imx**2+imy**2)
dirs = arctan2(imy, imx)

# Save as image
# pil_im = Image.fromarray(dirs)
# pil_im.convert("LA").save("dirs.png")

if args.format == "hslimage":
    # Map direction to Hue, Magnitude to value
    from math import pi
    maxmag = amax(magnitude)
    height, width = magnitude.shape
    im = Image.new("RGBA", (width, height))
    for y in range(height):
        p = int(math.ceil(float(y)/height*100))
        sys.stderr.write("\rCreating gradient HSL image... [{0}%]".format(p))
        sys.stderr.flush()
        for x in range(width):
            d = dirs[y][x]
            hue = ((d+pi) / (2 * pi)) * 360
            value = (magnitude[y][x]/maxmag)
            r, g, b = hsl_to_rgb(hue, 1.0, value)
            im.putpixel((x, y), (r, g, b, 255))
    sys.stderr.write("\n")
    im.save(out)
elif args.format == "magimage":
    pil_im = Image.fromarray(magnitude)
    pil_im.convert("LA").save(out)
elif args.format == "json":
    with open(out, "w") as f:
        json.dump({
            'dirs': dirs.tolist(),
            'magnitudes': magnitude.tolist()
        }, f)

# with open(os.path.join(path, base + ".gradient_mag.json"), "w") as f:


