jpg=$(shell ls *.jpg)
# grad=$(shell ls *.gradient.png)
grads=$(jpg:%.jpg=%.gradient.png)


all: $(grads)

%.gradient.png: %.jpg
	imagegradient $< --output $@

print-%:
	@echo '$*=$($*)'

links: $(jpg)
	python links.py $^